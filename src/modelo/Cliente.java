/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author gales
 */
public class Cliente {
    private String nome;
    private String sobrenome;
    private int rg;
    private int cpf;
    private String endereco;
    
    public Cliente(){
        this.nome="";
        this.sobrenome="";
        this.rg=0;
        this.cpf=0;
        this.endereco="";
        
    }

 
    
    /**
     * @return the nome
     */
    public String getNome() {
      return nome;
    }
    
    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
 
        /**
     * @return the sobrenome
     */
    public String getSobrenome() {
      return sobrenome;
    }
    
    /**
     * @param sobrenome the sobrenome to set
     */
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
    
        /**
     * @return the rg
     */
    public int getRg() {
      return rg;
    }
    
    /**
     * @param rg the rg to set
     */
    public void setRg(int rg) {
        this.rg = (short) rg;
    }
    
        /**
     * @return the cpf
     */
    public int getCpf() {
      return cpf;
    }
    
    /**
     * @param cpf the cpf to set
     */
    public void setCpf(int cpf) {
        this.cpf = (short) cpf;
    }
    
    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }
 
    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}
