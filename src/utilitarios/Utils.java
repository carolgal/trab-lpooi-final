/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utilitarios;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author gales
 */
public class Utils {
    
    static NumberFormat formatandoValores = new DecimalFormat("R$ #,##");
    
    public static String doubleToString(Double valor) {
        return formatandoValores.format(valor);
    }
    
    
}
