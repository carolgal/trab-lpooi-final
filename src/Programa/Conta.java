/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Programa;

import utilitarios.Utils;

/**
 *
 * @author gales
 */
public class Conta {
    
    private static int contadorDeContas = 1;
    
    private int numeroConta;
    private Cliente cliente;
    private Double saldo = 0.0;
    
    public Conta(Cliente cliente) {
        this.numeroConta = contadorDeContas;
        this.cliente = cliente;
        contadorDeContas += 1;
    }
    
    public int getNumeroConta() {
        return numeroConta;
    }
    
    public void setNumeroConta(int numeroConta) {
        this.numeroConta = numeroConta;
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public Double getSaldo() {
        return saldo;
    }
    
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
    
    public String toString() {
        return "\nNúmero da conta: " + this.getNumeroConta() +
            "\nNome: " + this.cliente.getNome() +
            "\nSobrenome: " + this.cliente.getSobrenome() +
            "\nCPF: " + this.cliente.getCpf() +
            "\nRG: " + this.cliente.getRg() + 
            "\nEndereço: " + this.cliente.getEndereco()+ 
            "\nSaldo: " + Utils.doubleToString(this.getSaldo()) +
            "\n";
        
    }
    
    public void depositar(Double valor) {
        if(valor > 0) {
            setSaldo(getSaldo() + valor);
            System.out.println("Depósito realizado com sucesso");
        }else {
            System.out.println("não foi possível realizar o depósito");
        }
        
    }
    
    public void sacar(Double valor) {
        if(valor > 0 && this.getSaldo() >= valor) {
            setSaldo(getSaldo() - valor);
            System.out.println("Saque realizado com sucesso");
        }else {
            System.out.println("não foi possível realizar o saque");
        }
    }
    
    public void tranferir(Conta contaParaDeposito, Double valor) {
        if(valor > 0 && this.getSaldo() >= valor) {
            setSaldo(getSaldo() - valor);
            
            contaParaDeposito.saldo = contaParaDeposito.getSaldo() + valor;
            System.out.println("Transferência relaizada");
        }else {
            System.out.println("não foi possivel realizar a transferencia");
        }
            
    }
    
}
