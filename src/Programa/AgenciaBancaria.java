/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Programa;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author gales
 */
public class AgenciaBancaria {
    static Scanner input = new Scanner(System.in);
    static ArrayList<Conta> contasBancarias;
    
    public static void main(String[] args) {
        contasBancarias = new ArrayList<Conta>();
        operacoes();
    }
    
    public static void operacoes() {
         System.out.println("------------------------------------------------------");
        System.out.println("-------------Bem vindos a nossa Agência---------------");
        System.out.println("------------------------------------------------------");
        System.out.println("***** Selecione uma operação que deseja realizar *****");
        System.out.println("------------------------------------------------------");
        System.out.println("|   Opção 1 - Criar conta   |");
        System.out.println("|   Opção 2 - Depositar     |");
        System.out.println("|   Opção 3 - Sacar         |");
        System.out.println("|   Opção 4 - Transferir    |");
        System.out.println("|   Opção 5 - Listar        |");
        System.out.println("|   Opção 6 - Sair          |");
                
        int operacao = input.nextInt();
        
        switch(operacao) {
            case 1:
                criarConta();
                break;
            case 2:
                depositar();
                break;
            case 3:
                sacar();
                break;
            case 4:
                transferir();
                break;
             case 5:
                listar();
                break;
             case 6:
                System.out.println("flw é nois");
                System.exit(0); 
             
             default:
                 System.out.println("opção inválida");
                 operacoes();
                 break;
        }
    }
    
    public static void criarConta() {
        
        System.out.println("\nNome: ");
        String nome = input.next();
        
        System.out.println("\nSobreome: ");
        String sobrenome = input.next();
        
        System.out.println("\nCPF: ");
        String cpf = input.next();
        
        System.out.println("\nRg: ");
        String rg = input.next();
        
        System.out.println("\nEndereço: ");
        String endereco = input.next();
        
        Cliente cliente = new Cliente(nome, sobrenome, cpf, rg, endereco);
        
        Conta conta = new Conta(cliente);
        
        contasBancarias.add(conta);
        System.out.println("Sua conta foi criada");
        
        operacoes();
    }
    
    private static Conta encontrarConta(int numeroConta) { //verificar a conta do usuario a partir do numero digitado 
        Conta conta = null;
        if(contasBancarias.size() > 0) {
            for(Conta c: contasBancarias) {
                if(c.getNumeroConta() == numeroConta) {
                conta = c;
                }
            }
        }
    
        return conta;
    }
     
    public static void depositar() {
        System.out.println("Número da conta: ");
        int numeroConta = input.nextInt();
        
        Conta conta = encontrarConta(numeroConta);
        
        if(conta != null) {
            System.out.println("Qual valor deseja depositar? ");
            Double valorDeposito = input.nextDouble();
            conta.depositar(valorDeposito);
            System.out.println("Valor depositado com sucesso");
        }else {
            System.out.println("conta não encontrada");
        }
        operacoes();
    }
    
    public static void sacar() {
        System.out.println("Número da conta: ");
        int numeroConta = input.nextInt();
        
        Conta conta = encontrarConta(numeroConta);
        
        if(conta != null) {
            System.out.println("Qual valor deseja sacar? ");
            Double valorSaque = input.nextDouble();
            conta.sacar(valorSaque);
           // System.out.println("Valor sacado com suceso");
        }else {
            System.out.println("conta não encontrada");
        }
        operacoes();
    }
    
    public static void transferir() {
        System.out.println("Número da conta de quem vai enviar: ");
        int numeroContaRemetente = input.nextInt();
        
        Conta contaRemetente = encontrarConta(numeroContaRemetente);
        
        if(contaRemetente != null) {
            System.out.println("Numero da conta de quem vai receber: ");
            int numeroContaDestinatario = input.nextInt();
            
            Conta contaDestinatario = encontrarConta(numeroContaDestinatario);
            
            if(contaDestinatario != null) {
                System.out.println("Valor da transferencia");
                Double valor = input.nextDouble();
                
                contaRemetente.tranferir(contaDestinatario, valor);
            }else {
                System.out.println("A conta pra deposito nao foi encontrada");
            }
        
        }else {
            System.out.println("Conta para tranferencia nao encontrada");
        }
        operacoes();
    }
    
    public static void listar() {
        if(contasBancarias.size() > 0) {
            for(Conta conta: contasBancarias) {
                System.out.println(conta);
            }
            }else {
                    System.out.println("Não há contas cadastradas");
                    
        } 
        operacoes();
        }
        
    }
    
    
    

