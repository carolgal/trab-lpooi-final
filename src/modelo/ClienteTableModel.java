/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


/**
 *
 * @author gales
 */
public class ClienteTableModel extends AbstractTableModel{

  
    private  String colunas[]={"Nome: ", "Scobrenome: "};
    private  List<Cliente> dados = new ArrayList();
    
    public ClienteTableModel() {
        
    }
    
    @Override
    public int getRowCount() {
        //retorna o total de linahs na tabela
        return dados.size();
    }
    
    @Override
    public int getColumnCount() {
        //retorna o total de colunas
        return colunas.length;
    }
   
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        //retorna o  valor conforme a coluna e linnha
        //pega os dados corrente da linha
        Cliente cliente = dados.get(rowIndex);
        
        //retorna o valor da coluna
        switch (columnIndex) {
        case 0 -> {
            return cliente.getNome();
            }
        case 1 -> {
            return cliente.getSobrenome();
            }
        default -> {
            return null;
            }
        
        }
    
    }
    
    @Override
    public String getColumnName(int index) {
        return this.colunas[index];
    }
    
    public boolean removeCliente(Cliente cliente) {
        boolean result = this.dados.remove(cliente);
        this.fireTableDataChanged();
        return result; //update jTable
    }
    
    public void adicionaCliente(Cliente cliente) {
        this.dados.add(cliente);
        this.fireTableDataChanged(); //update Jtable
    }
    
    public void setListaClientes(List<Cliente> clientes) {
        this.dados = clientes;
        this.fireTableDataChanged(); //update jTable
        
    }
    
    public void limpaTabela() {
        this.dados = new ArrayList();
        this.fireTableDataChanged(); //update jTable
    }
    
    public Cliente getCliente(int linha) {
        return dados.get(linha);
    }
    
}
