/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Programa;

/**
 *
 * @author gales
 */
public class Cliente {

    private static int counter = 1;
    
    private String nome;
    private String sobrenome;
    private String cpf;
    private String rg;
    private String endereco;
    
    public Cliente(String nome, String sobrenome, String cpf, String rg, String endereco) {
        
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        counter += 1;
    }
    
  
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
        
    }
    
    public String getSobrenome() {
        return sobrenome;
    }
    
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
    
    public String getCpf() {
        return cpf;
    }
    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public String getRg() {
        return rg;
    }
    
    public void setRg(String rg) {
        this.rg = rg;
    }
    
    public String getEndereco() {
        return endereco;
    }
    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    public String toString() {
        return "\nNome: " + this.getNome() +
                "\nSobrenome: " + this.getSobrenome() +
                "\nCPF: " + this.getCpf() +
                "\nRG: " + this.getRg() +
                "\nEndreço: " + this.getEndereco();
        
    
    }
    
    
    
}
